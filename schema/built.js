const json = JSON.stringify;
const backgroundOptions = [
  {"label": "Primary", "value": "bg-brand-primary"},
  {"label": "Secondary", "value": "bg-brand-secondary"},
  {"label": "Tertiary", "value": "bg-brand-tertiary"},
  {"label": "Accent 1", "value": "bg-accent-1"},
  {"label": "Accent 2", "value": "bg-accent-2"},
  {"label": "Accent 3", "value": "bg-accent-3"},
  {"label": "Accent 4", "value": "bg-accent-4"},
  {"label": "Accent 5", "value": "bg-accent-5"},
  {"label": "Gradient 1", "value": "bg-gradient-1"},
  {"label": "Gradient 2", "value": "bg-gradient-2"},
  {"label": "Gradient 3", "value": "bg-gradient-3"},
  {"label": "Gradient 4", "value": "bg-gradient-4"},
  {"label": "Gradient 5", "value": "bg-gradient-5"},
  {"label": "Gradient 6", "value": "bg-gradient-6"},
  {"label": "White", "value": "bg-white"},
  {"label": "Off White", "value": "bg-off-white"},
  {"label": "Grey", "value": "bg-grey"},
  {"label": "Off Black", "value": "bg-off-black"},
  {"label": "Black", "value": "bg-black"},
];

module.exports = {
  uikit: {
    backgroundOptions: json(backgroundOptions),
  }
};
