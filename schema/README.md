These are the data objects for ejs 

`default.js` must be present 
`[brand].js` files can be defined and are merged with data from default.js depending on the `BRAND` envirnmental variable. `[brand]` should equal `BRAND`
 

example useage in schema:
```{
  "type":      "select",
  "id":        "bg_color",
  "label":     "Background Color",
  "options": <%- uikit.backgroundOptions -%>,
  "default": "bg-brand-primary"
}```


NOTE: 
  - NOT live updating! Run `yarn start` or `yarn watch` after updating!
  - All properties in brand specific files must also exist in default