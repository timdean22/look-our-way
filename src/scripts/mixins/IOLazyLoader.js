export default {
  data() {
    return {
      loaded: false,
    };
  },
  mounted () {
    // Graceful degredation for IE
    if (!('IntersectionObserver' in window)) {
      this.loadComponent();
      return;
    }
    // setup observer to watch the component
    const observer = new IntersectionObserver(this.componentInView, { rootMargin: this.margin });
    observer.observe(this.$refs.observable);
  },
  methods: {
    componentInView(entries, observer) {
      if (observer) {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            this.loadComponent();
            if (!this.loaderShouldPersist) observer.unobserve(entry.target);
          } else {
            this.loaded = false;
          }
        })
      }
    },
    loadComponent() {
      this.loaded = true;
    }
  },
};
