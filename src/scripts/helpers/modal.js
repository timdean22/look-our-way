/** 
 * @file base logic for a modal
 * [ESC] closes modal
 * 
 * @prop {Boolean} open
 * @emits update:open
 */
export default {
  props: {
    open: {
      type: Boolean,
      default: false
    },
    id: {
      default: Math.random().toString(16).substring(2) // quick id
    }
  },
  computed: {
    syncedOpen: {
      get() {
        return this.open;
      },
      set(value) {
        this.$emit('update:open', value);
      },
    }
  },
  methods: {
    closeModalKeyPress(e) {
      if (e.keyCode === 27) {
        this.syncedOpen = false;
      }
    }
  },

  mounted() {
    window.addEventListener('keyup', this.closeModalKeyPress);
  },

  destroyed() {
    window.removeEventListener('keyup', this.closeModalKeyPress);
  },
}
