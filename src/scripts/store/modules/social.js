const state = {
  socialLinks: {},
};

const mutations = {
  SAVE_SOCIAL(state, socialLinks) {
    state.socialLinks = socialLinks;
  }
};

const actions = {
  saveSocialLinks({ commit }, { socialLinks }) {
    commit('SAVE_SOCIAL', socialLinks);
  },
};

export default { namespaced: true, state, mutations, actions };
