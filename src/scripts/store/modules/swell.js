// To track whether we are loading the swell data or not
let loading = false;
let attemptsLeft = 50;

const baseState = {
  all: [],
  customer: [],
};

const mutations = {
  ADD_SWELL_DATA(state, swellData) {
    state.all.unshift(swellData);
  },
  ADD_SWELL_CUSTOMER(state, customer) {
    state.customer.unshift(customer);
  },
};

const commitBaseSwellData = (commit) => {
  commit('ADD_SWELL_DATA', {
    redemptionOptions: window.swellAPI.getActiveRedemptionOptions(),
    activeCampaigns: window.swellAPI.getActiveCampaigns(),
    vipTiers: window.swellAPI.getVipTiers(),
  });
};

/**
 * Once Swell is loaded, need to check for when the customer info has loaded.
 */
const queueCustomerSetup = (commit) => {
  const customer = window.swellAPI.getCustomerDetails();
  if (customer.pointsEarned) {
    window.swellAPI.refreshCustomerDetails(() => {
      commitBaseSwellData(commit);
      commit('ADD_SWELL_CUSTOMER', customer);
    });
  } else if (attemptsLeft > 0) {
    attemptsLeft -= 1;
    setTimeout(() => queueCustomerSetup(commit), 250);
  }
};

/**
 * Because Swell doesn't fire events for us, need to periodically check for
 * whether the API is loaded. When loaded, then trigger function for
 * swell dependent component setup
 */
const queueSwellSetup = (commit) => {
  if (window.swellAPI) {
    try {
      window.swellAPI.getCustomerDetails();
      commitBaseSwellData(commit);
      queueCustomerSetup(commit);
    } catch (err) {
      setTimeout(() => queueSwellSetup(commit), 100);
    }
  } else {
    setTimeout(() => queueSwellSetup(commit), 250);
  }
};

const actions = {
  init({ commit }) {
    if (loading === false) {
      loading = true;
      queueSwellSetup(commit);
    }
  },
};

export default { namespaced: true, state: baseState, mutations, actions };
