import Icon from 'scripts/components/basic/Icon.vue';
import Modal from 'scripts/components/basic/Modal.vue';
import Tab from 'scripts/components/basic/Tab.vue';
import Tabs from 'scripts/components/basic/Tabs.vue';
import Accordion from 'scripts/components/basic/Accordion.vue';
import AccordionGroup from 'scripts/components/basic/AccordionGroup.vue';
import AccountSelectNav from 'scripts/components/customers/AccountSelectNav.vue';
import AccountPanel from 'scripts/components/customers/AccountPanel.vue';
import AccountNav from 'scripts/components/customers/AccountNav.vue';
import InstagramGallery from 'scripts/components/basic/InstagramGallery.vue';
import CheckboxInputGroup from 'scripts/components/forms/CheckboxInputGroup.vue';
import CustomerSelectInput from 'scripts/components/forms/CustomerSelectInput.vue';
import CustomerTextInput from 'scripts/components/forms/CustomerTextInput.vue';
import Toast from 'scripts/components/basic/Toast.vue';
import Carousel from 'scripts/components/basic/Carousel.vue';
import Slide from 'scripts/components/basic/Slide.vue';
import ImageGrid from 'scripts/components/basic/ImageGrid.vue';
import ScrollingText from 'scripts/components/basic/ScrollingText.vue';
import FullOverlay from 'scripts/components/basic/FullOverlay.vue';
import FeaturedBlogPosts from 'scripts/components/blog/FeaturedBlogPosts.vue';
import IoLazyLoader from 'scripts/components/basic/IoLazyLoader.vue';
import EditorialGrid from 'scripts/components/basic/EditorialGrid.vue';
import GalleryCarousel from 'scripts/components/basic/GalleryCarousel.vue';
import LogosCarousel from 'scripts/components/basic/LogosCarousel.vue';
import CollectionsGrid from 'scripts/components/basic/CollectionsGrid.vue';

import ReferalForm from 'scripts/components/rewards/ReferalForm.vue';

import VButton from 'scripts/components/buttons/VButton.vue';
import AddToCart from 'scripts/components/buttons/AddToCart.vue';
import VArrowButton from 'scripts/components/buttons/VArrowButton.vue';
import VLink from 'scripts/components/buttons/VLink.vue';
import LinkButton from 'scripts/components/buttons/LinkButton.vue';
import AsyncButton from 'scripts/components/buttons/AsyncButton.vue';

import Cart from 'scripts/components/cart/Cart.vue';
import Sidecart from 'scripts/components/cart/Sidecart.vue';
import AccountProperties from 'scripts/components/basic/AccountProperties.vue';
import Navbar from 'scripts/components/navigation/Navbar.vue';
import FooterToTop from 'scripts/components/navigation/FooterToTop.vue';
import SearchResults from 'scripts/components/search/SearchResults.vue';
import ProductTile from 'scripts/components/product/ProductTile.vue';
import ProductSwatchesSimple from 'scripts/components/product/ProductSwatchesSimple.vue';
import ProductValueProps from 'scripts/components/product/ProductValueProps.vue';
import TargetAreas from 'scripts/components/product/TargetAreas.vue';
import ProductPotential from 'scripts/components/product/ProductPotential.vue';
import SupplementalFacts from 'scripts/components/product/SupplementalFacts.vue';
import ProductDisplay from 'scripts/components/product/ProductDisplay.vue';
import Collections from 'scripts/components/pages/Collections.vue';
import FeaturedCollection from 'scripts/components/collection/FeaturedCollection.vue';
import CollectionHero from 'scripts/components/collection/CollectionHero.vue';
import CollectionBanner from 'scripts/components/collection/CollectionBanner.vue';
import BlogCategoryLinks from 'scripts/components/blog/BlogCategoryLinks.vue';
import CollectionGrid from 'scripts/components/collection/CollectionGrid.vue'
import Collection from 'scripts/components/collection/Collection.vue';
import VideoCarousel from 'scripts/components/video/VideoCarousel.vue';
import VideoModal from 'scripts/components/video/VideoModal.vue';
import About from 'scripts/components/pages/About.vue';
import AboutImageBannerBlock from 'scripts/components/sections/AboutImageBannerBlock.vue';
import Page404 from 'scripts/components/pages/Page404.vue';
import FeaturedOffersLink from 'scripts/components/featured-offers/FeaturedOffersLink.vue';

import KlaviyoForm from 'scripts/components/forms/KlaviyoForm.vue';
import NewsletterSignup from 'scripts/components/newsletter/NewsletterSignup.vue';

import FooterBourdain from 'scripts/components/footer/FooterBourdain.vue';
import FooterValueProps from 'scripts/components/footer/FooterValueProps.vue';
import FooterEmailCollection from 'scripts/components/footer/FooterEmailCollection.vue';
import FooterMain from 'scripts/components/footer/FooterMain.vue';
import FooterContactSocial from 'scripts/components/footer/FooterContactSocial.vue';
import FooterSubFooter from 'scripts/components/footer/FooterSubFooter.vue';

// UI Kit
import SelectInput from 'scripts/components/forms/SelectInput.vue';
import TextInput from 'scripts/components/forms/TextInput.vue';
import EmailInput from 'scripts/components/forms/EmailInput.vue';
import TextAreaInput from 'scripts/components/forms/TextAreaInput.vue';
import RadioInput from 'scripts/components/forms/RadioInput.vue';

//sections
import SplitWrapper from 'scripts/components/basic/SplitWrapper.vue';
import HeroCarousel from 'scripts/components/sections/Hero.vue';
import HeroBanner from 'scripts/components/sections/HeroBanner.vue';
import HeroBannerText from 'scripts/components/sections/HeroBannerText.vue';
import HeroBannerCarousel from 'scripts/components/sections/HeroBannerCarousel.vue';
import QuoteSection from 'scripts/components/sections/QuoteSection.vue';
import BlogSection from 'scripts/components/sections/BlogSection.vue';
import CollectionLayoutTiles from 'scripts/components/sections/CollectionLayoutTiles.vue';
import CollectionLayoutTilesTile from 'scripts/components/sections/CollectionLayoutTilesTile.vue';
import CollectionLayoutLinear from 'scripts/components/sections/CollectionLayoutLinear.vue';
import CollectionLayoutBlocks from 'scripts/components/sections/CollectionLayoutBlocks.vue';
import TestimonialBlocks from 'scripts/components/sections/TestimonialBlocks.vue';
import TestimonialCarousel from 'scripts/components/sections/TestimonialCarousel.vue';
import InThePress from 'scripts/components/sections/InThePress.vue';
import Instagram from 'scripts/components/sections/Instagram.vue';
import FiftyFifty from 'scripts/components/sections/FiftyFifty.vue';
import FeaturedProduct from 'scripts/components/sections/FeaturedProduct.vue';
import CollectionLinks from 'scripts/components/sections/CollectionLinks.vue';
import CollectionTileSlider from 'scripts/components/sections/CollectionTileSlider.vue';
import ProductSet from 'scripts/components/sections/ProductSet.vue';
import CollectionsFeatured from 'scripts/components/collection/CollectionsFeatured.vue';
import HowItWorks from 'scripts/components/sections/HowItWorks.vue';
import ColumnBlocks from 'scripts/components/sections/ColumnBlock.vue';

import TopHeader2 from 'scripts/components/header-2/TopHeader2.vue';
import Header2Container from 'scripts/components/header-2/Header2Container.vue';
import FeaturedOffersContainer from 'scripts/components/featured-offers/FeaturedOffersContainer.vue';
import ShopByPattern from 'scripts/components/sections/ShopByPattern.vue';
import EditorialSets from 'scripts/components/sections/EditorialSets.vue';

// Pages
import PlanetboxCompare from 'scripts/components/pages/PlanetboxCompare.vue';
import PlanetboxWholesaleForm from 'scripts/components/pages/PlanetboxWholesaleForm.vue';
import SubNav from 'scripts/components/pages/SubNav.vue';
import FAQs from 'scripts/components/pages/FAQs.vue';

// kit builder
import PlanetboxKitBuilder from 'scripts/components/pages/PlanetboxKitBuilder/PlanetboxKitBuilder.vue';

// dto
import Header2Data from 'scripts/components/dto/Header2Data.vue';
import SocialData from 'scripts/components/dto/SocialData.vue';
// Super Collections
import CollectionSuper from 'scripts/components/collection/CollectionSuper.vue';

/**
 * Deferred components
 *
 * These components will only load if they are rendered by markup. Use
 * for components that don't load on every page.
 *
 * i.e. below the fold components, landing page components, etc.
 */
// const AsyncComponent = () => import('scripts/components/category/AsyncComponent.vue')

export default {
  Icon,
  Modal,
  Tab,
  Tabs,
  Accordion,
  AccordionGroup,
  AccountSelectNav,
  AccountNav,
  AccountPanel,
  CheckboxInputGroup,
  CustomerSelectInput,
  CustomerTextInput,
  Toast,
  Carousel,
  Slide,
  IoLazyLoader,
  ImageGrid,
  VButton,
  AddToCart,
  LinkButton,
  VArrowButton,
  VLink,
  AsyncButton,
  FeaturedBlogPosts,
  Cart,
  Sidecart,
  Navbar,
  FooterToTop,
  SearchResults,
  InstagramGallery,
  ProductTile,
  ProductSwatchesSimple,
  TargetAreas,
  ProductValueProps,
  ProductPotential,
  SupplementalFacts,
  ProductDisplay,
  FeaturedCollection,
  CollectionGrid,
  CollectionHero,
  CollectionBanner,
  BlogCategoryLinks,
  Collections,
  GalleryCarousel,
  LogosCarousel,
  CollectionsGrid,
  Collection,
  VideoCarousel,
  VideoModal,
  NewsletterSignup,
  KlaviyoForm,
  About,
  AboutImageBannerBlock,
  Page404,
  FooterBourdain,
  FooterValueProps,
  FooterEmailCollection,
  FooterMain,
  FooterContactSocial,
  FooterSubFooter,
  FeaturedOffersLink,
  FullOverlay,
  EditorialGrid,

  // ui-kit
  SelectInput,
  TextInput,
  EmailInput,
  RadioInput,
  TextAreaInput,

  // sections
  SplitWrapper,
  HeroCarousel,
  HeroBanner,
  HeroBannerText,
  HeroBannerCarousel,
  QuoteSection,
  BlogSection,
  CollectionLayoutTiles,
  CollectionLayoutTilesTile,
  CollectionLayoutLinear,
  CollectionLayoutBlocks,
  TestimonialBlocks,
  TestimonialCarousel,
  InThePress,
  Instagram,
  FiftyFifty,
  FeaturedProduct,
  TopHeader2,
  Header2Container,
  FeaturedOffersContainer,
  CollectionsFeatured,
  ShopByPattern,
  CollectionLinks,
  CollectionTileSlider,
  EditorialSets,
  ProductSet,
  HowItWorks,
  ColumnBlocks,
  AccountProperties,
  ReferalForm,

  // pages
  PlanetboxCompare,
  PlanetboxWholesaleForm,
  SubNav,
  faqs: FAQs,

  // kit builder
  PlanetboxKitBuilder,

  // dto
  Header2Data,
  SocialData,

  // super collection
  CollectionSuper,
};
