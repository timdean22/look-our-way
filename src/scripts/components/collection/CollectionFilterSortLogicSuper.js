import without from 'lodash/without';
import defer from 'lodash/defer';
import intersection from 'lodash/intersection';
import pick from 'lodash/pick';
import { TAG_DELIMITER, SORT_OPTIONS, AVAILABILITY_FILTER_FNS, AVAILABILITY_FILTER_NAMES} from './collection-constants';
import ShopifyCollectionLogicOR, { getUrlEncodedSort, getUrlEncodedFilters, categorize, formatFilterList } from './ShopifyCollectionLogicOR';

const DEFAULT_AVAILABILITY_FILTER = AVAILABILITY_FILTER_NAMES[0];

export default {
  props: {
    collections: Array,
    /** Defines what filter categories are shown to the user */
    filterCategories: Array
  },
  data() {
    const activeFilters = getUrlEncodedFilters() || [];
    const activeSort = getUrlEncodedSort() || SORT_OPTIONS[0].value;
    const tagDelimiter = TAG_DELIMITER;
    const activeAvailabilityFilter = DEFAULT_AVAILABILITY_FILTER;
    let collectionControllers = [];

    // The first item in the collections array will have full collection data
    // create a controller 
    if(this.collections[0] && this.collections[0].collection) {
      collectionControllers.push(
        new ShopifyCollectionLogicOR(this.collections[0].collection)
          .filterBy(activeFilters)
          .setCustomFilters([AVAILABILITY_FILTER_FNS[activeAvailabilityFilter]])
          .sortBy(activeSort)
      )
    }
    
    // The remaining will only have handles
    // Here we get an array of handles to lazy load
    const remainingCollectionHandles = this.collections
      .map(c => c.collectionHandle)
      .filter(Boolean);

    // fetch the collections, intantiate each controller, and merge all with existing controller array
    this.fetchCollections(...remainingCollectionHandles)
      .then(collectionsData => {
        this.collectionControllers = [
          ...this.collectionControllers,
          ...collectionsData.map(collection => (
            new ShopifyCollectionLogicOR(collection)
            .filterBy(this.activeFilters)
            .setCustomFilters([AVAILABILITY_FILTER_FNS[this.activeAvailabilityFilter]])
            .sortBy(this.activeSort)
          ))
        ]
      })

    return {
      collectionControllers,
      activeFilters,
      activeSort,
      activeAvailabilityFilter,
      sortOptions: SORT_OPTIONS,
      tagDelimiter,
    }
  },
  methods: {
    onFilterToggle(tag) {
      let newActiveFilters;
      if(this.activeFilters.includes(tag)) {
        newActiveFilters = without(this.activeFilters, tag);
      } else {
        newActiveFilters = [...this.activeFilters, tag];
      }
      this.activeFilters = newActiveFilters;
      this.collectionControllers.forEach(controller => defer(controller.filterBy.bind(controller, newActiveFilters)));
    },
    onAvailabilityFilterToggle(value) {
      const defaultFilterFn = k => k;

      this.activeAvailabilityFilter = value;
      this.collectionControllers.forEach(controller => defer(controller.setCustomFilters.bind(controller, [AVAILABILITY_FILTER_FNS[value] || defaultFilterFn])))
    },
    clearAllFilters() {
      this.activeAvailabilityFilter = DEFAULT_AVAILABILITY_FILTER;
      this.activeFilters = [];
      this.collectionControllers.forEach(controller => defer(controller.clearFilters.bind(controller)));
    },
    onSortSelect(value) {
      if(value !== this.activeSort) {
        this.activeSort = value;
        this.collectionControllers.forEach(controller => defer(controller.sortBy.bind(controller, value)));
      }
    },
    fetchCollections (...handles) {
      const fetches = handles.map(handle => 
        fetch(`/collections/${handle}?view=json`)
          .then((res) => res.json())
      )

      return Promise.all(fetches)
        .catch(() => {
          this.$store.dispatch(
            'toast/send', 
            { text: 'Oh no, something went wrong. Please try again later.', type: 'error' }
          )
        })
    }
  },
  computed: {
    /** from all the tags in all the collections, reduce to the clients desired categories and order */
    filtersList() {
      const allTags = this.collectionControllers.flatMap(({ collection }) => collection && collection.all_tags || []);
      const allFiltersCategorized = categorize(allTags, TAG_DELIMITER);
      const categoriesToInclude = intersection(this.filterCategories, Object.keys(allFiltersCategorized)); // order determined by first array
      const reducedCategories = pick(allFiltersCategorized, categoriesToInclude);

      return formatFilterList(reducedCategories);
    },
    /**  format active filters for use as pill buttons */
    formattedActiveFilters() {
      return this.activeFilters.map(tag => {
        const [, value] = tag.split(this.tagDelimiter);
        return {
          value,
          remove: function() {
            this.activeFilters = without(this.activeFilters, tag);
            this.collectionControllers.forEach(controller => defer(controller.filterBy.bind(controller, this.activeFilters)));
          }.bind(this),
        }
      })
    }
  }
}
