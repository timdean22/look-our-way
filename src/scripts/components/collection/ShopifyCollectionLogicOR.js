/**
 * @file Loads all products upfront and filters products using logical OR client-side.
 *       Sorting is also performed client-side. Collection products should always be provided in
 *       "best selling" order by default.
 */
import uniq from 'lodash/uniq';

export const DEFAULT_COLLECTION = {
  handle: '',
  products: [],
}
export const DEFAULT_OPTIONS = {
  tagDelimiter: '::',
}

/**
 * @param {string[]} tags
 * @returns {{ [key: string]: string[] }} maps of categories with their filters
 */
export function categorize (tags = [], delimiter = '::') {
  tags = uniq(tags);
  return tags.reduce((categories, tag) => {
    if (!tag.includes(delimiter)) return categories;
    const tagInfo = tag.split(delimiter);
    const category = tagInfo[0];
    const tagFilter = tagInfo[1];
    if (!categories[category]) {
      categories[category] = [];
    }
    if (!categories[category].includes(tagFilter)) {
      categories[category].push(tagFilter);
    }
    return categories;
  }, {});
}

/**
 * @param {{[key: string]: string[]}} catgorizedFilters 
 * @returns {{category: string, filters: string[]}[]}
 */
export function formatFilterList (catgorizedFilters = {}) {
  const categoryArray = [];
  Object.keys(catgorizedFilters).forEach((category) => categoryArray.push({
    category,
    'filters': catgorizedFilters[category],
  }));
  return categoryArray;
}

export default class ShopifyCollectionLogicOR {
  constructor(collection = DEFAULT_COLLECTION, options = DEFAULT_OPTIONS) {
    this.collection = collection
    this.options = options;
    this.allProducts = this.collection.products.filter(product => !product.tags.includes('hide'));
    this.products = this.allProducts;
    this.allFilters = this._getAllFilters(this.products);
    this.lastSortKey = null;
    this.lastTags = null;
    this.customFilters = [] // Function[]
  }

  setCustomFilters (fnArray) {
    this.customFilters = fnArray;
    this.filterBy(this.lastTags);
    return this;
  }

  filterBy(tags = []) {
    const filters = categorize(tags, this.options.tagDelimiter);
    let filteredProductList = [...this.allProducts];

    Object.entries(filters).forEach(([category, categoryFilters]) => {
      if (categoryFilters.length === 0) return;
      categoryFilters = categoryFilters.map(value => category + this.options.tagDelimiter + value);
      filteredProductList = filteredProductList.filter((product) => {
        // more checks here
        return product.tags.some((tag) => categoryFilters.includes(tag));
      });
    });

    filteredProductList = this._filterByCustomFilters(filteredProductList);
    this.products = this._sort(filteredProductList, this.lastSortKey);
    setUrlEncodedFilters(tags);
    this.lastTags = tags;
    return this;
  }

  /** clear custom filter functions and tag filters */
  clearFilters() {
    this.customFilters = [];
    this.tags = []
    this.lastTags = []
    this.filterBy([]);
  }

  sortBy(key) {
    if(!key) return this;
    this.products = this._sort(this.products, key);
    this.lastSortKey = key;
    setUrlEncodedSort(key);
    return this;
  }

  _filterByCustomFilters (products) {
    return this.customFilters.reduce((acc, filterFn) => {
      return acc.filter(filterFn);
    }, [...products]);
  }

  _sort (products = [], key) {
    return [...products].sort((a, b) => {
      if (key === 'title-ascending') return a.title.toLowerCase().localeCompare(b.title.toLowerCase());
      if (key === 'title-descending') return b.title.toLowerCase().localeCompare(a.title.toLowerCase());
      if (key === 'price-ascending') return a.price - b.price;
      if (key === 'price-descending') return b.price - a.price;
      if (key === 'created-ascending') return toDate(a.created_at) - toDate(b.created_at);
      if (key === 'created-decending') return toDate(b.created_at) - toDate(a.created_at);
      // default to best seller or manual order
      return a.order - b.order;
    });
  }

  _getAllFilters (products = []) {
    // build the master list of the categories and their filters
    const categories = categorize(products.flatMap(product => product.tags), this.options.tagDelimiter);
    // convert to an array a template can iterate over
    return formatFilterList(categories);
  }
}

export function setUrlEncodedFilters (tags) {
  const params = new URLSearchParams(window.location.search);
  if(!tags) return;

  if(tags.length < 1) {
    params.delete('filters');
  }

  if(tags.length > 0) {
    params.set('filters', tags.join(','))
  }

  history.replaceState(null, null, '?' + params.toString());
}

export function setUrlEncodedSort (value) {
  const params = new URLSearchParams(window.location.search);

  if(!value) {
    params.delete('sort');
  } else {
    params.set('sort', value)
  }

  history.replaceState(null, null, '?' + params.toString());
}

export function getUrlEncodedSort () {
  const params = new URLSearchParams(window.location.search);
  return params.get('sort');
}

export function getUrlEncodedFilters () {
  const params = new URLSearchParams(window.location.search);
  const filters = params.get('filters') || '';
  
  return filters.split(',').filter(Boolean);
}

function toDate(secondsString) {
  return new Date(parseInt(secondsString, 10) * 1000);
}
